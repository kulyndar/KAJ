## Spuštění lokálně

### Prerekvizity

1. Node JS

### Build a run

1. otevřít  CMD (nebo bash) ve složce projektu
2. spustit ```npm install```
3. spustit ```npm run build```
4. spustit ```npm run```
5. aplikace běží na ```localhost:8080```

