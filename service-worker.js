
self.addEventListener('install', function(e) {
 e.waitUntil(
   caches.open('airhorner').then(function(cache) {
     return cache.addAll([
       '/',
       '/index.html',
       '/bundle.js',
       '/styles/style.css',
       '/styles/animation.css',
       '/styles/media.css',
       '/svg/first.svg',
       '/svg/KAJ2.svg',
       '/svg/second.svg',
       '/svg/third.svg',
       '/svg/spinner.svg',
       '/svg/2.svg',
       '/svg/4.svg',
       '/styles/back1.jpg',
      "avatar/avatar-beard.png",
       "avatar/avatar-girl.png",
       "avatar/avatar-king.png",
       "avatar/avatar-office.png",
       "avatar/avatar-sw.png",
       "avatar/avatar-old.png",
       "avatar/avatar-pilot.png",
       "avatar/avatar-student.png",
       "avatar/avatar-wonderwoman.png",
       'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
       '/audio/lose.wav',
       '/audio/win.wav',
       '/audio/play.wav',
       '/audio/merge.wav'
     ]);
   })
 );
});
self.addEventListener('fetch', function(event) {

console.log(event.request.url);

event.respondWith(

caches.match(event.request).then(function(response) {
if(navigator.onLine){
  return fetch(event.request);
}
return  response ||fetch(event.request);

})

);

});
