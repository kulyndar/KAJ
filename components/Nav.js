import React from "react";
import { Link } from "react-router-dom";
import { count } from "./Home.js";
import Svg from "./svg.js";
import Home from "./Home";

/*global variable, that controls audio playing*/
export var sound = true;
/*Navigation class*/
export default class NavBar extends React.Component {
  constructor() {
    super();
    this.state={
      sound:true
    }
  }

  render() {

    const buttons = (
      <nav><div className="container-button">
        <button className="bskew bskew--skew bskew-default" onClick={()=>{location.hash="home"}}>
          <span>Home</span>
        </button>
        <button className="bskew bskew--skew bskew-default"   onClick={()=>{location.hash="results"}}>
          <span>Results</span>
        </button>
        <div className="volume">{!this.state.sound?<span className="glyphicon glyphicon-volume-off" aria-hidden="true" onClick={this.volumeHandler}></span>:<span className="glyphicon glyphicon-volume-up" aria-hidden="true" onClick={this.volumeHandler}></span>}</div>
      </div>
        </nav>);
          return buttons;

  }
  /*Online/offline detection*/
  componentDidMount(){
    if(!navigator.onLine){
    setTimeout(()=>  swal("You are offline...!", "... but don`t worry, you can enjoy the game even offline!", "success"),500);
    }
  }
  /*Switch off/on sound*/
  volumeHandler=(e)=>{
    if(e.currentTarget.classList.contains("glyphicon-volume-up")){
      sound=false;
      this.setState({sound:false});
    }else{
      sound=true;
      this.setState({sound:true});
    }
  }





}
