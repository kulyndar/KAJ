import React from "react";
import ReactDOM from "react-dom";
import NavBar from "./Nav.js";
import Home from "./Home.js";
import Results from "./ResultList.js";
import { HashRouter, Route, Switch } from "react-router-dom";
import DBManager from "./game-components/Manager";
import swal from "sweetalert";


/*Main class, that controls the whole application*/
class Main extends React.Component {
  constructor() {
    super();
  }
  render() {

    return (
      <HashRouter hashType="noslash">
        <div className="content">
          <NavBar />
          <div className="under-nav">
            <Switch>
              <Route exact path="/" component={Home} />
              <Route path="/home" component={Home} />
              <Route path="/playgame" component={Home} />
              <Route path="/results" component={Results} />

            </Switch>
          </div>
        </div>
      </HashRouter>);

  }


}
ReactDOM.render(<Main />, document.getElementById("content"));
