import React from "react";
import PropTypes from "prop-types";
import Tile from "./game-components/Tile";
import Decoder from "./game-components/InputDecoder";
import DBManager from "./game-components/Manager";
import Sound from "./game-components/Sound";
import {
  sound
} from "./Nav";
/*Gloval variabled for game logic*/
var moved = false;
var merged = [];
var currentMoves = 0;
/*Class that controls the game*/
export default class Game extends React.Component {

  constructor(props) {
    super(props);
    //props: username, image
    this.state = {
      tiles: new Array(16),
      over: false,
      score: 0,
      win: false,
      username: this.props.username,
      hscore: 0,
      image: this.props.image,
      waiting: true

    }
    this.mounted = false;
    /*Audio*/
    this.soundMerge = new Sound("/audio/merge.wav");
    this.soundWin = new Sound("/audio/win.wav");
    this.soundLose = new Sound("/audio/lose.wav");
    /*Init game*/
    this.state.tiles.fill(null);
    setTimeout(() => this.getRandomTiles(3), 100);
    setTimeout(() => {
      this.getHScore()
    }, 1000);
    /*Add event Listeners for moving*/
    window.addEventListener("keydown", function(e) {
      // space and arrow keys
      if ([32, 37, 38, 39, 40].indexOf(e.keyCode) > -1) {
        e.preventDefault();
      }
    }, false);
    window.addEventListener("keydown", (e) => {
      this.move(e.which);
    });

  }
componentWillUnmount(){
    this.mounted = false;
}
componentWillMount(){
  this.mounted = true;

}
  render() {

    let res = [];
    for (var i = 0; i < 4; i++) {
      res.push(<div className="row" key={"row" + i}>{this.getRow(i * 4)}</div>);
    }

    const game = (<div className="game-container">
      <div className={this.state.over||this.state.win?"filter":"filter-none"}></div>
      <div className={this.state.over||this.state.win?"filter-button":"filter-button-none"} onClick={this.restart}>{this.state.win?"Winner! Score is "+this.state.score:"Game over"}<br /><span className="glyphicon glyphicon-repeat" aria-hidden="true"></span></div>
      <div className="game">{res}</div>
    </div>);

    const spinner = isEdge ? <div className="loader-edge small"></div> : <img className="spinner" src="/svg/spinner.svg" />;

    return (
      <div name="playgame">
        <div className="user">
          <img className="avatar" src={this.state.image} />
          <span className="glyphicon glyphicon-question-sign help" aria-hidden="true" onClick={this.props.help}></span>
          <div>Name: {this.state.username}</div>
          <div>High score: {this.state.waiting?spinner:this.state.hscore}</div>
          <div>Score: {this.state.score}</div>
        </div>
        {game}
      </div>
    );
  }
  /*Checks if there is tile 2048*/
  checkWin = () => {
    var tiles = this.state.tiles;
    for (var i = 0; i < 16; i++) {
      if (tiles[i] && tiles[i].props.value == 2048) {
        /*Player wins the game*/
        this.setState({
          win: true
        });
        if (sound) {
          this.soundWin.play();
        }
        /*Save current score if it is bigger than high score*/
        if (this.state.score > this.state.hscore) {
          var user = {
            username: this.state.username,
            image: this.state.image,
            score: this.state.score
          };
          /*Puts user in IndexedDB*/
          DBManager.write(user);
          this.setState({
            hscore: this.state.score
          });
        }
        return;
      }
    }
  }
  /*Reads user`s high score, if player is presented in DB`*/
  getHScore = () => {
    DBManager.getUser(this.state.username, (result) => {
      if (result) {
        var hscore = result.score;
      } else {
        var hscore = 0;
      }
      var waiting = false;
      this.setState({
        hscore,
        waiting
      });
    });
  }
  /*Restart game after win or lose*/
  restart = () => {
    var score = 0;
    var over = false;
    var tiles = new Array(16);
    var win = false;
    tiles.fill(null);
    this.setState({
      score,
      over,
      tiles,
      win
    });
    setTimeout(() => this.getRandomTiles(3), 100);

  }
  /*Making game grid*/
  getRow = (start) => {
    let res = [];
    for (var i = start; i < start + 4; i++) {
      res.push(<div className="cell" key={i}>{
          this.state.tiles[i]
            ? this.state.tiles[i]
            : <div></div>
        }</div>);
    }
    return res;
  }
  /*Move handler*/
  move = (code) => {
    if(!this.mounted){
      return ;
    }
    /*decodes event*/
    let dir = Decoder.decode(code);
    /*variable for timeout (is needed for animation)*/
    const wait = 30;

    switch (dir) {
      case "up":
        /*Tiles that do not have up neighbour*/
        if (currentMoves > 10) {
          console.log("many");
          return;
        }
        currentMoves++;
        var missed = [0, 1, 2, 3];
        /*moving logic*/
        for (var i = 0; i < 3; i++) {
          const b = i;
          setTimeout(() => {
            this.moveDir(missed, -4, "up", b + 1)
          }, i * wait);
        }

        break;
      case "down":
        /*Tiles that do not have down neighbour*/
        if (currentMoves > 10) {
          console.log("many");
          return;
        }
        currentMoves++;
        var missed = [12, 13, 14, 15];
        /*moving logic*/
        for (var i = 0; i < 3; i++) {
          const b = i;
          setTimeout(() => {
            this.moveDir(missed, 4, "down", b + 1)
          }, i * wait);
        }
        break;
      case "right":
        /*Tiles that do not have right neighbour*/
        if (currentMoves > 10) {
          console.log("many");
          return;
        }
        currentMoves++;
        var missed = [3, 7, 11, 15];
        /*moving logic*/
        for (var i = 0; i < 3; i++) {
          const b = i;
          setTimeout(() => {
            this.moveDir(missed, 1, "right", b + 1)
          }, i * wait);
        }
        break;
      case "left":
        /*Tiles that do not have left neighbour*/
        if (currentMoves > 10) {
          console.log("many");
          return;
        }
        currentMoves++;
        var missed = [0, 4, 8, 12];
        /*moving logic*/
        for (var i = 0; i < 3; i++) {
          const b = i;
          setTimeout(() => {
            this.moveDir(missed, -1, "left", b + 1)
          }, i * wait);
        }
        break;
      case "restart":
        if (this.state.win || this.state.over) {
          this.restart();
        }
        break;
      default:
        break;
    }
  }
  /*Movind logic.
  parameter: missed - tiles that cannot be moved
            trgRule - where is the target
            dir - direction of move, string
            iter - number of current iteration   */
  moveDir = (missed, trgRule, dir, iter) => {
    let tiles = this.state.tiles;
    for (var i = 0; i < 16; i++) {
      if (!missed.includes(i)) {
        /*if cell has tile*/
        if (tiles[i]) {
          /*if target is empty*/
          if (tiles[i + trgRule] == null) {
            this.pureMove(i, i + trgRule, dir);
            moved = true;
          } else {
            /*if target is not empty, checks if tiles has the same values to merge */
            if (this.isMerge(i, i + trgRule)) {
              this.merge(i, i + trgRule, dir);
              moved = true
            }
          }
        }
      }
    }
    currentMoves--;
    /*if something happened*/
    if (moved && iter == 3) {
      /*if there is no moves*/
      if (!this.state.tiles.includes(null) && !this.checkHavePlace() && iter == 3) {
        this.gameOver();
      }
      moved = false;
      merged = [];

      var random = Math.floor(Math.random() * (3 - 0)) + 0
      this.getRandomTiles(random);

    } else {
      if (!this.state.tiles.includes(null) && !this.checkHavePlace() && iter == 3) {
        this.gameOver();
      }
    }
    /*check if there is tile 2048*/
    if (iter == 3) {
      this.checkWin();
    }
  }
  /*check if there is any available move*/
  checkHavePlace = () => {
    var place = false;
    for (var i = 0; i < 16; i++) {
      /*rules for moving in different direction*/
      var LEFT = i % 4 == 0 ? null : i - 1;
      var UP = i <= 3 ? null : i - 4;
      var RIGHT = (i + 1) % 4 == 0 ? null : i + 1;
      var DOWN = i >= 12 ? null : i + 4;

      var t = this.state.tiles;
      if (this.isMerge(i, LEFT) || this.isMerge(i, UP) || this.isMerge(i, RIGHT) || this.isMerge(i, DOWN)) {
        place = true;
      }
    }
    return place;
  }
  /*Game over handler*/
  gameOver = () => {
    /*Plays audio*/
    if (sound) {
      this.soundLose.play();
    }
    this.setState({
      over: true
    });
    /*Writes user in DB, id player`s current score is higher than high score - score is changed in DB`*/
    if (this.state.score > this.state.hscore) {
      var user = {
        username: this.state.username,
        image: this.state.image,
        score: this.state.score
      };
      DBManager.write(user);
      this.setState({
        hscore: this.state.score
      });
    }

  }
  /*Checks if 2 tiles can be merged*/
  isMerge = (src, trg) => {
    /*If they are both not empty */
    if (this.state.tiles[src] && this.state.tiles[trg]) {
      /*and if their values are the same*/
      if (this.state.tiles[src].props.value == this.state.tiles[trg].props.value) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
  /*Merges 2 tiles*/
  merge = (src, trg, dir) => {
    if (merged.includes(trg)) {
      return;
    }
    if (sound) {
      this.soundMerge.play();
    }
    merged[trg] = trg;
    let res = this.state.tiles;
    res[src] = <Tile value={res[src].props.value} />;
    this.setState({
      tiles: res
    });
    var val = this.state.tiles[trg].props.value * 2;
    var result = this.state.tiles;
    result[trg] = <Tile value={val} direction={dir}/>
    result[src] = null;
    /*Change players score*/
    this.addScore(val);
    /*Update grid*/
    this.setState({
      tiles: result
    });

  }
  /*Moving tile */
  pureMove = (src, trg, dir) => {
    let res = this.state.tiles;
    res[src] = <Tile value={res[src].props.value} />;
    this.setState({
      tiles: res
    });
    var val = this.state.tiles[src].props.value;
    var result = this.state.tiles;
    result[trg] = <Tile value={val} direction={dir} />
    result[src] = null;
    this.setState({
      tiles: result
    });
  }
  /*changes player`s score`*/
  addScore = (val) => {
    this.setState({
      score: (parseInt(this.state.score) + val)
    })
  }
  /*gets N random tiles to the grid*/
  getRandomTiles = (n) => {
    let size = 0;
    let res = this.state.tiles;
    /*Count availible cells*/
    this.state.tiles.forEach((t) => {
      if (t == null) {
        ++size;
      }
    });
    /*can add new tile only to availible cells*/
    if (size && size < n) {
      n = size;
    }

    for (var i = 0; i < n; i++) {
      /*tile 2 or 4*/
      var twoOrFour = Math.random();
      do {
        /*random place*/
        var place = Math.floor(Math.random() * (16 - 0)) + 0;
      } while (res[place] != null);
      if (twoOrFour < 0.7) {
        res[place] = <Tile value={2} new />;
      } else {
        res[place] = <Tile value={4} new />;
      }
    }
    this.setState({
      tiles: res
    });
  }

}
