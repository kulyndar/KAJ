import React from "react";
import Game from "./Game.js";
import Tabs from "./Tabs.js";
import Svg from "./svg.js";
import Sound from "./game-components/Sound";
import {
  sound
} from "./Nav";
import {Modal, Button} from "react-bootstrap";


export var count = 0;
/*Home page*/
export default class Home extends React.Component {
  constructor() {
    super();
    this.state = {
      "count": count,
      "username": "Anonymus",
      image: "/avatar/avatar-beard.png",
      play: location.hash=="#playgame",
      visible: location.hash != "" || isEdge,
      help:false
    }
    /*Audio*/
    this.playSound = new Sound("/audio/play.wav");

  }
  componentWillUpdate(){
    if(location.hash!="#playgame"&&this.state.play){
      this.setState({play:false});
    }
  }

  /*Handler for image chooser in Tabs*/
  tabsImageHandler = (e) => {
    var element = e.currentTarget;
    var image = "/avatar/avatar-" + element.classList[1] + ".png";
    this.setState({
      image
    });
    document.querySelector(".chosen").classList.remove("chosen");
    element.classList.add("chosen")
  }
  /*Handler for input field in Tabs*/
  tabsInputHandler = (e) => {
    if (e.currentTarget.value == "") {
      return;
    }
    this.setState({
      username: e.currentTarget.value
    });
  }
  /*Handler for Play button in Tabs*/
  tabsSubmitHandler = (e) => {
    location.hash="#playgame";
    if (sound) {
      this.playSound.play();
    }
    e.currentTarget.classList.add("go");
    document.querySelector(".tabs").classList.add("dissapear");
    setTimeout(() => {
      this.setState({
        play: true
      });
      window.scrollTo(0, document.body.scrollHeight);
    }, 1000);

  }
  /*Handler for intro animated SVG*/
  visibleHandler = (e) => {
    document.querySelectorAll(".dissapear").forEach((item) => item.classList.remove("dissapear"));
    e.currentTarget.classList.add("dissapear");
    setTimeout(() => this.setState({
      visible: true
    }), 1000);

  }
  render() {
    if (!this.state.visible) {
      return (<div><Svg handler={this.visibleHandler} />
        <div className="home dissapear" >
          <h1>Welcome to 2<sup>11</sup> game!</h1>

          <Tabs changeHandler={this.tabsInputHandler} imageHandler={this.tabsImageHandler} submit={this.tabsSubmitHandler} />

        </div>
    </div>);
    }
    return (<div className="home" >
      <h1>Welcome to 2<sup>11</sup> game!</h1>
      {this.getModal()}
      {this.state.play?<Game username={this.state.username} image={this.state.image} help={this.handleHelp} /> : <Tabs changeHandler={this.tabsInputHandler} imageHandler={this.tabsImageHandler} submit={this.tabsSubmitHandler} />}

    </div>)
  }
  handleHelp=()=>{
    this.setState({help:true});
  }
  getModal=()=>{
    const modal=(
      <Modal show={this.state.help} onHide={this.handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>How to play?</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h3>Controls</h3>
          <p>Simply play with your keyboard. You can use buttons <b>W</b>, <b>A</b>, <b>S</b>, <b>D</b> or <b>arrows</b>.</p>
          <hr />
          <h3>What happens?</h3>
          <p>When you merge tiles with the same value, new tile is created</p>
          <div className="example">
            <img  className="ex ex-four" src="/svg/4.svg" />
            <img className="ex ex-two-src" src="/svg/2.svg" />
            <img className="ex ex-two-trg" src="/svg/2.svg" />
          </div>
          <hr />
          <h3>Goal</h3>
        <p>Your goal is to make a tile with value <b>2048</b></p>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.handleClose}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
    return modal;
  }
  handleClose=()=>{
    this.setState({help:false});
  }


}
