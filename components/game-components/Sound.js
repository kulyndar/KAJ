/*Conrols audio playing*/
export default class Sound{
  constructor(src){
    /*creates <audio>*/
    this.sound = document.createElement("audio");
    this.sound.src = src;
    this.sound.setAttribute("preload", "auto");
    this.sound.setAttribute("controls", "none");
    this.sound.style.display = "none";
    document.body.appendChild(this.sound);

  }
  /*API for play and stop audio*/
  play=()=>{
      this.sound.play();
  }
  stop=()=>{
      this.sound.pause();
  }
}
