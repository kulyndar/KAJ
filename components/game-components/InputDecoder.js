/*decoder of moving events*/
class Decoder {
  constructor() {
    this.map = { //W A S D + arrows
      87: "up", //W
      65: "left", //A
      83: "down", //S
      68: "right", //D

      38: "up", //up arrow
      37: "left", //left arrow
      40: "down", //down arrow
      39: "right", //right arrow

      13: "restart"
    }
  }
  decode=(code)=>{
    return this.map[code];
  }
}
const INSTANCE = new Decoder();
export default INSTANCE;
