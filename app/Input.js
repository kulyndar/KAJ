"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _Home = require("./Home.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var InputContent = function (_React$Component) {
  _inherits(InputContent, _React$Component);

  function InputContent() {
    _classCallCheck(this, InputContent);

    var _this = _possibleConstructorReturn(this, (InputContent.__proto__ || Object.getPrototypeOf(InputContent)).call(this));

    _this.state = {
      "name": "",
      "image": ""
    };
    _this.handleClick = _this.handleClick.bind(_this);

    return _this;
  }

  _createClass(InputContent, [{
    key: "render",
    value: function render() {
      var content = _react2.default.createElement(
        "div",
        { className: _Home.count < 1 ? "tabs none" : "tabs" },
        _react2.default.createElement(
          "ul",
          { className: "links" },
          _react2.default.createElement(
            "li",
            { className: "first link activeTab" },
            _react2.default.createElement(
              "a",
              { href: "#", onClick: this.handleClick },
              "1"
            )
          ),
          _react2.default.createElement(
            "li",
            { className: "second link" },
            _react2.default.createElement(
              "a",
              { href: "#", onClick: this.handleClick },
              "2"
            )
          ),
          _react2.default.createElement(
            "li",
            { className: "third link" },
            _react2.default.createElement(
              "a",
              { href: "#", onClick: this.handleClick },
              "3"
            )
          )
        ),
        _react2.default.createElement(
          "div",
          { className: "tab" },
          _react2.default.createElement(
            "div",
            { className: "first display" },
            "Enter your name/nickname"
          ),
          _react2.default.createElement(
            "div",
            { className: "second" },
            "Choose image"
          ),
          _react2.default.createElement(
            "div",
            { className: "third" },
            "Button: play"
          )
        )
      );
      return content;
    }
  }, {
    key: "handleClick",
    value: function handleClick(e) {
      /*find target tab*/
      var to = e.target.parentNode.classList[0];
      document.querySelector(".display").classList.remove("display");
      /*changes for animation*/
      if (document.querySelector(".activeTab")) {
        document.querySelector(".activeTab").classList.remove("activeTab");
      }
      var current = e.target.parentNode;
      current.classList.add("activeTab");
      document.querySelectorAll(".visited").forEach(function (i) {
        return i.classList.remove("visited");
      });
      var previous = current.previousElementSibling;
      while (previous) {
        previous.classList.remove("activeTab");
        previous.classList.add("visited");
        previous = previous.previousElementSibling;
      }
      /*show target*/
      document.querySelector("." + to + ":not(.link)").classList.add("display");
      /*prevent Default*/
      return false;
    }
  }]);

  return InputContent;
}(_react2.default.Component);

exports.default = InputContent;
//# sourceMappingURL=../Input.js.map