"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.count = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _Game = require("./Game.js");

var _Game2 = _interopRequireDefault(_Game);

var _Input = require("./Input.js");

var _Input2 = _interopRequireDefault(_Input);

var _svg = require("./svg.js");

var _svg2 = _interopRequireDefault(_svg);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var count = exports.count = 0;

var Home = function (_React$Component) {
  _inherits(Home, _React$Component);

  function Home() {
    _classCallCheck(this, Home);

    var _this = _possibleConstructorReturn(this, (Home.__proto__ || Object.getPrototypeOf(Home)).call(this));

    _this.state = {
      "count": count
    };

    return _this;
  }

  _createClass(Home, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var nav = document.querySelector("nav.none");
      if (!nav) {
        this.setState({ "count": exports.count = count += 1 });
      }
    }
  }, {
    key: "render",
    value: function render() {
      return _react2.default.createElement(
        "div",
        { className: "home", onClick: function onClick() {
            if (count < 1) {
              var svg = document.querySelector("svg");
              svg.classList.add("smaller");
              document.querySelectorAll(".none").forEach(function (e) {
                return e.classList.add("show");
              });
            }
            exports.count = count += 1;
          } },
        count < 1 ? _react2.default.createElement(_svg2.default, null) : _react2.default.createElement("div", null),
        _react2.default.createElement(
          "h1",
          { className: count < 1 ? "none" : "" },
          "Game Header"
        ),
        _react2.default.createElement(_Input2.default, null)
      );
    }
  }]);

  return Home;
}(_react2.default.Component);

exports.default = Home;
//# sourceMappingURL=../Home.js.map