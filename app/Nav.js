"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _reactRouterDom = require("react-router-dom");

var _Home = require("./Home.js");

var _svg = require("./svg.js");

var _svg2 = _interopRequireDefault(_svg);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NavBar = function (_React$Component) {
  _inherits(NavBar, _React$Component);

  function NavBar() {
    _classCallCheck(this, NavBar);

    var _this = _possibleConstructorReturn(this, (NavBar.__proto__ || Object.getPrototypeOf(NavBar)).call(this));

    _this.state = {
      "active": "",
      "count": _Home.count
    };
    _this.clickHandler = _this.clickHandler.bind(_this);

    return _this;
  }

  _createClass(NavBar, [{
    key: "componentWillMount",
    value: function componentWillMount() {
      var hash = location.hash;
      hash = hash.substr(2);
      console.log(hash);
      if (hash == "") {
        hash = "home";
      } else {
        this.setState({ "count": 1 });
      }
      console.log(hash);
      this.setState({ "active": hash });
    }
  }, {
    key: "render",
    value: function render() {
      console.log(this.state.count);
      var navigation = _react2.default.createElement(
        "nav",
        { className: "navigation none" },
        _react2.default.createElement(
          "ul",
          null,
          this.state.count < 1 ? _react2.default.createElement(
            "div",
            { id: "svg" },
            _react2.default.createElement("li", null)
          ) : _react2.default.createElement(
            "div",
            { id: "svg" },
            _react2.default.createElement(
              "li",
              null,
              _react2.default.createElement(_svg2.default, { id: "svg-nav" })
            )
          ),
          _react2.default.createElement(
            "li",
            { id: "home", className: this.state.active === "home" ? "active" : "" },
            _react2.default.createElement(
              _reactRouterDom.Link,
              { to: "/", onClick: this.clickHandler },
              "Home"
            )
          ),
          _react2.default.createElement(
            "li",
            { id: "results", className: this.state.active === "results" ? "active" : "" },
            _react2.default.createElement(
              _reactRouterDom.Link,
              { to: "/results", onClick: this.clickHandler },
              "Results"
            )
          ),
          _react2.default.createElement(
            "li",
            { id: "doc", className: this.state.active === "doc" ? "active" : "" },
            _react2.default.createElement(
              _reactRouterDom.Link,
              { to: "/", onClick: this.clickHandler },
              "Documentation"
            )
          ),
          _react2.default.createElement(
            "li",
            { id: "tut", className: this.state.active === "tut" ? "active" : "" },
            _react2.default.createElement(
              _reactRouterDom.Link,
              { to: "/results", onClick: this.clickHandler },
              "Game Tutorial"
            )
          )
        )
      );
      return navigation;
    }
  }, {
    key: "clickHandler",
    value: function clickHandler(e) {
      var newActive = e.target.parentNode.id;
      this.setState({ "active": newActive });

      this.setState({ "count": this.state.count + 1 });
    }
  }]);

  return NavBar;
}(_react2.default.Component);

exports.default = NavBar;
//# sourceMappingURL=../Nav.js.map